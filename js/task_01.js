function Poster(props) {
  const wrapClass = "poster";
  return (
      <div className="poster__item">
        <div className={`${wrapClass}__img-wrap`}>
          <img className={`${wrapClass}__img`} src={props.img} alt={props.name} />
        </div>
        <h3 className={`${wrapClass}__heading`}>
          Название: <a href = {
            props.img
          }
          className = {
            `${wrapClass}__link`
          }
          target = "_blank" > {
            props.name
          } </a>
        </h3>
        <div className={`${wrapClass}__year`}>
          Год: {props.year}
        </div>
      </div>
  );
}

ReactDOM.render(
  <div className="poster">
    <Poster name="Tron Legacy" year="2010" img="img/tron.jpg" />
    <Poster name="Guardians of the galaxy" year="2014" img="img/guardians.jpg" />
    <Poster name="Star Wars" year="1977" img="img/star-wars.jpg" />
  </div>,
  document.getElementById('app')
);