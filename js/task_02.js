const Header = function() {
  return(
    <header className="header">
      <div className="container">
        <nav className="top-nav">
          <menu className="top-nav__list">
            <li className="top-nav__item">
              <a href="#" className="top-nav__link">
                Docs
              </a>
            </li>
            <li className="top-nav__item">
              <a href="#" className="top-nav__link">
                Tutorial
              </a>
            </li>
            <li className="top-nav__item">
              <a href="#" className="top-nav__link">
                Community
              </a>
            </li>
            <li className="top-nav__item">
              <a href="#" className="top-nav__link">
                Blog
              </a>
            </li>
          </menu>
        </nav>
      </div>
    </header>
  )
};

const Banner = function() {
  return (
    <div className="banner">
    <div className="container">
      <h1 className="banner__heading">ReactJS</h1>
      <div className="banner__desc">A JavaScript library for building user interfaces</div>
      <div className="banner__readmore">
        <a href="#" className="banner__btn btn">Get Started</a>
        <a href="#" className="banner__link link">Take the Tutorial</a>
      </div>
    </div>
  </div>
  )
}

const Footer = function () {
  return (
    <footer className="footer">
      <div className="container">
        <span>React</span> forever!
      </div>
    </footer>
  )
}

const Main = function () {
  return (
    <main className="content">
      <div className="container">
        <div className="content__item">
          <h3 className="content__heading">Declarative</h3>
          <div className="content__desc">
            <p>
              React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.
            </p>
            <p>
              Declarative views make your code more predictable and easier to debug.
            </p>
          </div>
        </div>
        <div className="content__item">
          <h3 className="content__heading">Component-Based</h3>
          <div className="content__desc">
            <p>
              React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.
            </p>
            <p>
              Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.
            </p>
          </div>
        </div>
        <div className="content__item">
          <h3 className="content__heading">Learn Once, Write Anywhere</h3>
          <div className="content__desc">
            <p>
              We don’t make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code.
            </p>
            <p>
              React can also render on the server using Node and power mobile apps using React Native.
            </p>
          </div>
        </div>
      </div>
    </main>
  )
}

const App = function() {
  return (
    <div className="wrapper">
      <Header />
      <Banner />
      <Main />
      <Footer />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('app'));